import React from 'react';

const Page = props => {
  return (
    <div className="nbx-page">
      <span className="drag-handle">Page</span>
      <div className="nbx-page-children">
        {props.children}
      </div>
    </div>
  )
};

export default Page;