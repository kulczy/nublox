import React from 'react';

const Row = props => {
  return (
    <div className="nbx-row">
      <span className="drag-handle">Row</span>
      <div className="nbx-row-children">
        {props.children}
      </div>
    </div>
  )
};

export default Row;