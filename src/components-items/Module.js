import React from 'react';

const Module = props => {
  return (
    <div className="nbx-module" data-nbx-id={props.item.id}>
      <span className="drag-handle">{props.item.settings.title}</span>
      <div className="nbx-module-children">
        {props.children}
      </div>
    </div>
  )
};

export default Module;