import React from 'react';
import SortBox from '../components-utils/SortBox';

const Column = props => {
  const _onSort = info => {
    props.item.sort(info);
  }

  const _onAdd = () => {
    props.item.add(props.item.id);
  }

  return (
    <div className={'nbx-column-' + props.item.settings.size_lg}>
      Column
      <SortBox boxID={props.item.id} boxGroup="column" onSort={_onSort.bind(this)} handle=".drag-handle">
        {props.children}
      </SortBox>
      <div onClick={_onAdd}>ADD MODULE</div>
    </div>
  )
};

export default Column;