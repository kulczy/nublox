import React from 'react';
import SortBox from '../components-utils/SortBox';

const Container = props => {
  const _onSort = info => {
    props.item.sort(info);
  }

  return (
    <div className="nbx-container">
      Container
      <SortBox boxID={props.item.id} boxGroup="row" onSort={_onSort.bind(this)} handle=".drag-handle">
        {props.children}
      </SortBox>
    </div>
  )
};

export default Container;