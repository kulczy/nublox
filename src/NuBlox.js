import React from 'react';
import PropTypes from 'prop-types';
import NuBloxCore from './NuBloxCore';
import './NuBlox.css';

import generate from './model/items.model';

import Modal from './components-utils/Modal';
import Page from './components-items/Page';
import Container from './components-items/Container';
import Row from './components-items/Row';
import Column from './components-items/Column';
import Module from './components-items/Module';

class NuBlox extends NuBloxCore {
  constructor() {
    super();
    this.moduleTypes = [
      { type: 'text', name: 'Text' },
      { type: 'html', name: 'HTML' },
    ];
  }

  componentWillMount() {
    // Save input prop as object   
    const initItems = this.props.input.length ? this._toObject(this.props.input) : generate.generatePage();
    this.setState({ items: initItems });
  }

  render() {
    // Return single item (recursive)
    const renderItem = (item) => {
      const components = { page: Page, container: Container, row: Row, column: Column, module: Module }; // Available item components
      const CurrentItem = components[item.type]; // Set current item type to render as component
      // Props passed to items components
      const props = {
        id: item.id, 
        settings: item.settings,
        sort: this.sort.bind(this),
        add: this.add.bind(this),
        remove: this.remove.bind(this)
      };
      return (
        <CurrentItem key={item.id} item={props}>
          {'children' in item && item.children.map((child, i) => renderItem(child))}
        </CurrentItem>
      );
    }    
    
    return (
      <div>
        {renderItem(this.map)}
        <Modal modules={this.moduleTypes} add={this.add.bind(this)}></Modal>
      </div>
    );
  }
}

NuBlox.propTypes = {
  input: PropTypes.string.isRequired,
  output: PropTypes.any
}

export default NuBlox;

// TODO validate props