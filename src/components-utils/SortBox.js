import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Sortable from 'sortablejs';

class SortBox extends Component {
  componentDidMount() {
    Sortable.create(this.refs.box, {
      group: this.props.boxGroup,
      animation: 150,
      handle: this.props.handle,
      onAdd: e => {
        this.props.onSort({
          itemID: e.item.attributes.getNamedItem('data-nbx-id').value,
          oldBoxID: e.from.attributes.getNamedItem('data-nbx-sort-id').value,
          oldIndex: e.oldIndex,
          newBoxID: e.to.attributes.getNamedItem('data-nbx-sort-id').value,
          newIndex: e.newIndex
        });
      },
      onUpdate: e => {
        this.props.onSort({
          itemID: e.item.attributes.getNamedItem('data-nbx-id').value,
          oldBoxID: e.from.attributes.getNamedItem('data-nbx-sort-id').value,
          oldIndex: e.oldIndex,
          newBoxID: e.to.attributes.getNamedItem('data-nbx-sort-id').value,
          newIndex: e.newIndex
        });
      }
    });
  }

  render() {
    return (
      <div ref="box" data-nbx-sort-id={this.props.boxID}>
        {this.props.children}
      </div>
    )
  }
}

SortBox.propTypes = {
  boxID: PropTypes.string.isRequired,
  boxGroup: PropTypes.string,
  onSort: PropTypes.func.isRequired,
  handle: PropTypes.string
}

SortBox.defaultProps = {
  boxGroup: null,
  handle: null
};

export default SortBox;