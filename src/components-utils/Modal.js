import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Modal extends Component {
  constructor() {
    super();

    this.state = {
      show: true
    }
  }

  _onAddModule() {
    this.props.add();
  }

  render() {
    return (
      <div>
        {this.props.modules.map((module, i) => (
          <div key={i} onClick={this._onAddModule}>{module.name}</div>
        ))}
      </div>
    );
  }
}

Modal.propTypes = {
  modules: PropTypes.array.isRequired,
  add: PropTypes.func.isRequired
};

export default Modal;