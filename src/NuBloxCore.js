import { Component } from 'react';
import update from 'immutability-helper';
import generate from './model/items.model';

class NuBloxCore extends Component {
  constructor() {
    super();
    this.state = { items: null }
  }

  // Sort items
  sort(info) {
    const oldBoxIndex = this._findIndexById(info.oldBoxID); // Find index of old sorted item box
    const newBoxIndex = this._findIndexById(info.newBoxID); // Find index of new sorted item box
    // Create new items object with removed sorted item from box children
    let newItems = update(this.state.items, {
      [oldBoxIndex]: { children: { $apply: children => children.filter(child => child !== info.itemID) } }
    });
    // Update new items object with sorted item in new place
    newItems = update(newItems, {
      [newBoxIndex]: { children: { $splice: [[info.newIndex, 0, info.itemID]] } }
    });
    this.setState({ items: newItems });
  }

  // Add item
  add(wrapper) {
    const newItem = generate.generateModule('text'); // Generate new item
    const wrapperIndex = this._findIndexById(wrapper); // Find index of wrapper
    // Add new item to object
    let newItems = update(this.state.items, {
      $push: [newItem]
    });
    // Add id to children
    newItems = update(newItems, {
      [wrapperIndex]: { children: { $push: [newItem.id] } }
    });
    this.setState({ items: newItems });
  }

  remove() {

  }

  // Items map to render
  get map() {
    return this._createMap(this._findByType('page'));
  }

  // Return new sorted items object with nested children
  _createMap(item) {
    const itemCopy = Object.assign({}, item);
    itemCopy.children = itemCopy.children.map(id => this._createMap(this._findById(id)));
    return itemCopy;
  }

  // Find item by ID
  _findById(id) {
    return this.state.items.find(item => item.id === id);
  }

  // Find item by type
  _findByType(type) {
    return this.state.items.find(item => item.type === type);
  }  

  // Find item index by ID
  _findIndexById(id) {
    return this.state.items.findIndex(item => item.id === id);
  }

  // Decode items to object
  _toObject(str) {
    return JSON.parse(decodeURIComponent(str));
  }

  // Encode items to string
  _toString(obj) {
    return encodeURIComponent(JSON.stringify(obj));
  }  

}

export default NuBloxCore;
