const i = {
  page: {
    id: null,
    type: 'page',
    children: [],
    settings: { title: 'Page' }
  },

  container: {
    id: null,
    type: 'container',
    children: [],
    settings: { title: 'Container' }
  },

  row: {
    id: null,
    type: "row",
    children: [],
    settings: { title: 'Row' }
  },

  column: {
    id: null,
    type: 'column',
    children: [],
    settings: { title: 'Column', size_lg: '12', size_md: '12', size_sm: '12', size_xs: '12' }
  },

  module: {
    id: null,
    type: 'module',
    children: [],
    settings: { title: 'Module' },
    moduleType: null
  }
}

const newID = () => {
  return Date.now() + Math.random().toString().substr(10);
}

const generatePage = () => {
  const column    = Object.assign({}, i.column, { id: newID() });
  const row       = Object.assign({}, i.row, { id: newID(), children: [column.id] });
  const container = Object.assign({}, i.container, { id: newID(), children: [row.id] });
  const page      = Object.assign({}, i.page, { id: newID(), children: [container.id] });
  return [page, container, row, column];
}

const generateModule = (type) => {
  return Object.assign({}, i.module, { id: newID(), moduleType: type });
}

export default { generatePage, generateModule };