import React from 'react';
import ReactDOM from 'react-dom';
import NuBlox from './NuBlox';

const nuBloxSettings = {
  input: document.getElementById('nublox-input').value,
  output: document.getElementById('nublox-output')
}

ReactDOM.render(<NuBlox { ...nuBloxSettings }/>, document.getElementById('nublox'));
